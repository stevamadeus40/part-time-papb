package com.parttime.parttime;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

public class CategoryActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout llPengajar, llMakan, llBersih, llTransportasi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        llPengajar = (LinearLayout) findViewById(R.id.kategori_pengajar);
        llMakan = (LinearLayout) findViewById(R.id.kategori_makan);
        llBersih = (LinearLayout) findViewById(R.id.kategori_bersih);
        llTransportasi = (LinearLayout) findViewById(R.id.kategori_transportasi);
        llPengajar.setOnClickListener(this);
        llMakan.setOnClickListener(this);
        llBersih.setOnClickListener(this);
        llTransportasi.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.kategori_pengajar:
                Toast.makeText(this, "Klik kategori Pengajar",
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.kategori_makan:
                Toast.makeText(this, "Klik kategori Tempat Makan",
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.kategori_bersih:
                Toast.makeText(this, "Klik kategori Kebersihan",
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.kategori_transportasi:
                Toast.makeText(this, "Klik kategori Transportasi",
                        Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
