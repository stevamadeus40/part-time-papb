package com.parttime.parttime;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button LoginBtn;
    private TextView SignupTxt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        LoginBtn = (Button) findViewById(R.id.tombol_login);
        SignupTxt = (TextView) findViewById(R.id.direct_signup);
        LoginBtn.setOnClickListener(this);
        SignupTxt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tombol_login:
                Toast.makeText(this, "Logging in...", Toast.LENGTH_SHORT).show();
                startActivity(CategoryActivity.class);
                finish();
                break;
            case R.id.direct_signup:
                startActivity(SignupActivity.class);
                break;
        }
    }

    private void startActivity(Class<?> cls) {
        Intent intent = new Intent(LoginActivity.this, cls);
        startActivity(intent);
    }
}
